# Proyecto_2

*El codigo del programa esta incompleto y por lo tanto su funcionalidad es 
limitada. 

Este programa cuenta con un generador de laberintos aleatorios que, cada vez 
que se ejecute el programa, un laberinto único será creado.
En adición a esto en el programa está implementado un resolvedor, que a partir 
de los algoritmos que se usaron, destaca la ruta menos extensa hacia la salida
en color morado(*)

(*)Para interactuar con el laberinto, use las teclas direccionales para mover al
jugador, desde la entrada que es presentada en color rojo, hasta la salida
marcada en color verde.

En el caso de estancarse dentro del laberinto (*) presione la tecla 's' para 
activar el resolverdor y mostrar la solución del laberinto